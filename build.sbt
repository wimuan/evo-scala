val Http4sVersion = "0.20.15"
val CirceVersion = "0.11.0"
val Specs2Version = "4.1.0"
val LogbackVersion = "1.2.3"

lazy val root = (project in file("."))
  .settings(
    organization := "com.example",
    name := "evo-websocket-demo",
    version := "0.0.1",
    scalaVersion := "2.12.6",
    libraryDependencies ++= Seq(
      "org.http4s"      %% "http4s-blaze-server" % Http4sVersion,
      "org.http4s"      %% "http4s-blaze-client" % Http4sVersion,
      "org.http4s"      %% "http4s-circe"        % Http4sVersion,
      "org.http4s"      %% "http4s-dsl"          % Http4sVersion,
      "org.specs2"      %% "specs2-core"         % Specs2Version % "test",
      "ch.qos.logback"  %  "logback-classic"     % LogbackVersion,

      "io.circe" %% "circe-core" % CirceVersion,
      "io.circe" %% "circe-generic" % CirceVersion,
      "io.circe" %% "circe-generic-extras" % CirceVersion,
      "io.circe" %% "circe-parser" % CirceVersion,
      "io.circe" %% "circe-parser" % CirceVersion,
      "io.circe" %% "circe-fs2" % CirceVersion,

      "com.typesafe.slick" %% "slick" % "3.3.1",
      "com.typesafe.slick" %% "slick-hikaricp" % "3.3.1",
      "org.slf4j" % "slf4j-nop" % "1.7.26",
      "org.xerial" % "sqlite-jdbc" % "3.7.2",
      "com.github.t3hnar" %% "scala-bcrypt" % "4.1",

      "org.scalactic" %% "scalactic" % "3.1.1",
      "org.scalatest" %% "scalatest" % "3.1.1" % "test"
    ),
    addCompilerPlugin("org.typelevel" %% "kind-projector"     % "0.10.3"),
    addCompilerPlugin("com.olegpy"    %% "better-monadic-for" % "0.3.0")
  )

scalacOptions ++= Seq(
  "-deprecation",
  "-encoding", "UTF-8",
  "-language:higherKinds",
  "-language:postfixOps",
  "-feature",
  "-Ypartial-unification",
  "-Xfatal-warnings",
)

// scalafix
scalacOptions ++= Seq("-Yrangepos")
/* scalacOptions ++= Seq("-Yrangepos", "-Ywarn-unused-import") // screws up console */
// addCompilerPlugin(scalafixSemanticdb)

cancelable in Global := true

/*
initialCommands in console := """
  |import example.model._
  |import example.state._
""".trim.stripMargin
*/
