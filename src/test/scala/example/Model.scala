import org.scalatest.funsuite.AnyFunSuite

import example.model._

import io.circe._
import io.circe.parser._
import io.circe.syntax._

import Codec._

class MessageTest extends AnyFunSuite {
  def msg(m: Msg) = m

  test("Message $type") {
    assert(msg(Ping(0xcafe)).asJson.noSpaces == """{"seq":51966,"$type":"ping"}""")
  }

  test("Ping.pong") {
    assert(Ping(1).pong == Pong(1))
  }
}
