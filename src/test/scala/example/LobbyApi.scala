import org.scalatest.funsuite.AnyFunSuite

import cats.effect._
import fs2.concurrent.Queue
import fs2.Stream
import io.circe._
import io.circe.parser._
import io.circe.syntax._
import scala.concurrent.ExecutionContext

import example.lobby_api._
import example.model._

class LobbyApiTest extends AnyFunSuite {
  implicit val contextShift: ContextShift[IO] = IO.contextShift(ExecutionContext.global)

  def process(m: MsgInput*) =
    Queue.unbounded[IO, Msg].flatMap(q =>
      LobbyApi[IO](q).map(_.process(Stream(m: _*).evalMap(IO(_)))))

  def output(i: MsgInput*): List[Msg] =
    process(i: _*).flatMap(_.compile.toList).unsafeRunSync

  test("ping") {
    assert(output(Ping(0xbeef)) == List(Pong(0xbeef)))
  }

  test("authorization") {
    assert(output(Login("", ""), Debug(None)).tail == List(NotAuthorized()))
  }
}
