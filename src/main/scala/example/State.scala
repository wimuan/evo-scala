package example.state

import cats._
import scala.concurrent.ExecutionContext.Implicits.global
import slick.jdbc.SQLiteProfile.api.{Table => DbTable, _}

/* connsole imports

import slick.jdbc.SQLiteProfile.api.{Table => DbTable, _}
import example.state.DbModel._
import example.state.DbImplicits._
import scala.concurrent.Await
import scala.concurrent.duration._
import cats.effect._
def run[R](a: slick.dbio.DBIOAction[R,slick.dbio.NoStream,Nothing]): IO[R] =
  IO.pure(Await.result(db.run(a), 3 seconds))

*/

// no type aliases at the top level, fixed in scala 3
object DbModel {
  case class User(id: Option[Int], username: String, user_type: String, password: String)

  class Users(tag: Tag) extends DbTable[User](tag, "users") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def username = column[String]("username")
    def user_type = column[String]("user_type")
    def password = column[String]("password")
    def * = (id.?, username, user_type, password).mapTo[User]
  }

  case class BaseTable[A](id: A, name: String, participants: Int)

  type Table = BaseTable[Option[Int]]
  type TableInput = BaseTable[Int]

  implicit val functorBT: Functor[BaseTable] = new Functor[BaseTable] {
    def map[A, B](bt: BaseTable[A])(f : A => B): BaseTable[B] =
      bt.copy(id = f(bt.id))
  }

  implicit val traverseBT: Traverse[BaseTable] = new Traverse[BaseTable] {
    def traverse[G[_]: Applicative, A, B](bt: BaseTable[A])(f : A => G[B]): G[BaseTable[B]] =
      Applicative[G].map(f(bt.id))(b => bt.copy(id = b))

    // I was asked to provide these, so "I did".
    def foldLeft[A, B](fa: BaseTable[A], b: B)(f: (B, A) => B): B = ???
    def foldRight[A, B](fa: BaseTable[A], lb: Eval[B])(f: (A, Eval[B]) => Eval[B]): Eval[B] = ???
  }
}

import DbModel._

class Tables(tag: Tag) extends DbTable[Table](tag, "tables") {
  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def name = column[String]("name")
  def participants = column[Int]("participants")
  def * = (id.?, name, participants) <>
    ((BaseTable[Option[Int]] _).tupled, (BaseTable.unapply[Option[Int]] _))
}

object DbImplicits {
  lazy val db = Database.forConfig("db.default")
  lazy val users = TableQuery[Users]
  lazy val tables = TableQuery[Tables]
}

object Db {
  import DbImplicits._

  var password1234 = "$2a$10$mmwpsvu31/4ox.ilqgI60eQk6G0XwDAF1GzbSKM.9DlmMmbNPP6N6"
  val defaultAdmin = User(None, "admin1234", "admin", password1234)
  val defaultUser = User(None, "user1234", "user", password1234)

  def createUser(user: User) = users.
    filter(_.username === user.username).
    length.result.flatMap {
      case 0 => users += user
      case _ => DBIO.successful(0)
    }

  def plans() = DBIO.seq(
    users.schema.createIfNotExists,
    tables.schema.createIfNotExists,
    createUser(defaultAdmin),
    createUser(defaultUser)
  )

  def setup() = db.run(plans())
}
