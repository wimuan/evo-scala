package example.model

import io.circe._
import io.circe.generic.extras.{Configuration, semiauto}
import io.circe.generic.extras.Configuration.default

import example.state._
import example.state.DbModel._

// all the big superclasses
sealed trait Msg extends Product with Serializable

sealed trait MsgInputs extends Msg
sealed trait MsgInput extends MsgInputs
sealed trait MsgAuthed extends MsgInputs
sealed trait MsgModifs extends MsgInputs

final case class DebugSwallow() extends MsgInput
final case class Debug(msg: Option[String]) extends MsgAuthed

final case class ApiError(code: Int, msg: String) extends Msg

final case class Pong(seq: Int) extends Msg
final case class Ping(seq: Int) extends MsgInput {
  def pong: Pong = Pong(seq)
}

final case class Login(username: String, password: String) extends MsgInput
final case class LoginFailed() extends Msg
final case class LoginSuccessful(user_type: String) extends Msg

final case class NotAuthorized() extends Msg

final case class SubscribeTables() extends MsgAuthed
final case class UnsubscribeTables() extends MsgAuthed

final case class AddTable(after_id: Int, table: TableInput) extends MsgModifs
final case class UpdateTable(table: TableInput) extends MsgModifs
final case class RemoveTable(id: Int) extends MsgModifs

object Codec {
  object ModelCodec {
    private implicit val conf: Configuration = default.withSnakeCaseConstructorNames

    implicit val encodeTable: Encoder[Table] = semiauto.deriveEncoder
    implicit val decodeTable: Decoder[Table] = semiauto.deriveDecoder
    implicit val encodeTableI: Encoder[TableInput] = semiauto.deriveEncoder
    implicit val decodeTableI: Decoder[TableInput] = semiauto.deriveDecoder
  }

  import ModelCodec._

  implicit val conf: Configuration = default.
    withDiscriminator("$type").
    withSnakeCaseConstructorNames

  implicit val encodeMsg: Encoder[Msg] = semiauto.deriveEncoder
  implicit val decodeMsg: Decoder[Msg] = semiauto.deriveDecoder

  implicit val encodeMsgInput: Encoder[MsgInputs] = semiauto.deriveEncoder
  implicit val decodeMsgInput: Decoder[MsgInputs] = semiauto.deriveDecoder
}
