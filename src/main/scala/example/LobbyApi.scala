package example.lobby_api

import cats.data._
import cats.effect._
import cats.effect.concurrent.Ref
import cats.implicits._
import com.github.t3hnar.bcrypt._
import fs2.concurrent.Topic
import fs2.{io => _, _}
import io.circe.fs2._
import io.circe.Json
import io.circe.syntax._
import scala.concurrent.Await
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global
import slick.jdbc.SQLiteProfile.api.{Session => _, Table => _, _}

import example.model._
import example.state._
import example.state.DbModel._
import example.session._
import example.session.Types._

object OptionEnrichment {
  implicit class FlippableOption[A](o: Option[A]) {
    def flip[B](b: => B) = o.fold(Option(b))(_ => None)
  }
}

object Option_ {
  // available in scala 2.13 https://stackoverflow.com/q/19690531
  def when[R](b: Boolean)(r: R) = if (b) Some(r) else None
}

// covariance was added to help type inference in process' case block
// methods toposorted by call graph, least elements up top
class LobbyApi[F[+_]](
  sref: Ref[F, Session[F]],
  updates: Updates[F]
)(implicit F: Concurrent[F]) {
  import DbImplicits._
  import OptionEnrichment._

  def authorizedType(user_types: String*)(a: F[Option[Msg]]): F[Option[Msg]] = for {
    authorized <- sref.get.map(s => s.authorized && user_types.exists(_ == s.user.get.user_type))
    msg <- if (authorized) a else F.pure(Some(NotAuthorized()))
  } yield msg

  def authorized = authorizedType("user", "admin") _
  def authorizedAdmin = authorizedType("admin") _

  def silent[A](a: F[A]): F[Option[Msg]] = a.map(_ => None)

  val routes : MsgInputs => F[Option[Msg]] = {
    case ds: DebugSwallow => F.pure(None)
    case d: Debug => sref.get.map(s => Some(Debug(Some(s.toString))))

    case p: Ping => F.pure(Some(p.pong))
    case l: Login => processLogin(l).map(Some(_))

    case SubscribeTables() => authorized(silent(subscribeTables))
    case UnsubscribeTables() => authorized(silent(unsubscribeTables))

    case at: AddTable => complainorOrPublish(addTable _, at)
    case ut: UpdateTable => complainorOrPublish(updateTable _, ut)
    case rt: RemoveTable => complainorOrPublish(removeTable _, rt)
  }

  def complainorOrPublish[A, M <: MsgModifs](
    f: M => EitherT[F, ApiError, A], m: M
  ): F[Option[Msg]] = f(m).value.flatMap({
    case Left(error) => F.pure(Some(error))
    case Right(_) => silent(updates.publish1(Some(m)))
  })

  def handleRoute : MsgInputs => F[Option[Msg]] = {
    case i: MsgInput => routes(i)
    case a: MsgAuthed => authorized(routes(a))
    case m: MsgModifs => authorizedAdmin(routes(m))
  }

  def updatesFiltered: Stream[F, Option[Msg]] =
    updates.subscribe(10).evalMap(m =>
      sref.get.map(s =>
        m.flatMap(Option_.when(s.subscribed))))

  def process: Pipe[F, MsgInputs, Msg] =
    _.evalMap(handleRoute).unNone.merge(updatesFiltered.unNone)

  //

  def run[R](a: slick.dbio.DBIOAction[R,slick.dbio.NoStream,Nothing]): F[R] =
    F.pure(Await.result(db.run(a), 3 seconds))

  //

  def processLogin(l: Login): F[Msg] =
    validateLogin(l).flatMap(u => u match {
      case Some(u) => for {
        _ <- sref.modify(s => (s.copy(authorized = true, user = Some(u)), s))
      } yield LoginSuccessful(u.user_type)

      case None => F.pure(LoginFailed())
    })

  def validateLogin(l: Login): F[Option[User]] = {
    for {
      user <- run(users.filter(_.username === l.username).result.headOption)
      valid <- F.pure(l.password.isBcryptedSafe(user map(_.password) getOrElse "").getOrElse(false))
    } yield user.filter(_ => valid)
  }

  //

  def subscribeTables: F[Unit] = sref.modify(s => (s.copy(subscribed = true), ()))
  def unsubscribeTables: F[Unit] = sref.modify(s => (s.copy(subscribed = false), ()))

  //

  def optToLeft[B](o : Option[B], a : ApiError): EitherT[F, ApiError, Unit] =
    EitherT(F.pure(o map(_ => Right(())) getOrElse(Left(a))))

  def errNotFound = ApiError(101, "not found")

  // after_id is assumed to be a UI feature, so it's not used
  def addTable(req: AddTable) : EitherT[F, ApiError, Unit] = for {
    table <- EitherT.right(run(tables.filter(_.id === req.table.id).result.headOption))
    _ <- optToLeft(table flip (()), ApiError(100, "id taken"))
    _ <- EitherT.right(run(tables forceInsert req.table.map(Some(_))))
  } yield ()

  def updateTable(req: UpdateTable): EitherT[F, ApiError, Unit] = for {
    query <- EitherT.right(F.pure(tables.filter(_.id === req.table.id)))
    table <- EitherT.right(run(query.result.headOption))
    _ <- optToLeft(table, errNotFound)
    _ <- EitherT.right(run(query.update(req.table.map(Some(_)))))
  } yield ()

  def removeTable(req: RemoveTable): EitherT[F, ApiError, Unit] = for {
    deletions <- EitherT.right(run(tables.filter(_.id === req.id).delete))
    _ <- EitherT(F.pure(if (deletions === 0) Left(errNotFound) else Right(())))
  } yield ()
}

object LobbyApi {
  def apply[F[+_]](
    updates: Updates[F],
  )(implicit F: Concurrent[F]): F[LobbyApi[F]] = {
    val session = Session[F](false, false, None)

    for (sref <- Ref.of[F, Session[F]](session))
      yield new LobbyApi[F](sref, updates)
  }
}
