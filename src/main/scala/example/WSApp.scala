package example.wsapp

import cats.effect._
import cats.effect.concurrent.Ref
import cats.implicits._
import fs2.concurrent.Queue
import fs2.{io => _, _} // could be solved with _root_, but this is shorter
import io.circe.fs2._
import io.circe.Json
import io.circe.syntax._
import org.http4s._
import org.http4s.dsl.Http4sDsl
import org.http4s.implicits._
import org.http4s.server.blaze.BlazeServerBuilder
import org.http4s.server.websocket._
import org.http4s.websocket.WebSocketFrame
import org.http4s.websocket.WebSocketFrame.{Ping => _, Pong => _, _}

import example.lobby_api._
import example.model._
import example.state._
import example.state.DbModel._
import example.session._

class WSApp[F[+_]](implicit F: ConcurrentEffect[F], timer: Timer[F])
    extends Http4sDsl[F] {

  import Codec._

  def pipeWsStrings: Pipe[F, WebSocketFrame, String] =
    _.collect { case Text(msg, _) => msg }

  def pipeWsJson: Pipe[F, WebSocketFrame, Json] =
    stringStreamParser[F] compose pipeWsStrings

  def pipeWsMsg[A](implicit decode: io.circe.Decoder[A]): Pipe[F, WebSocketFrame, A] =
    _.through(decoder[F, A] compose pipeWsJson)

  def pipeWsStr: Pipe[F, Any, WebSocketFrame] = _.collect { case a => Text(a.toString) }

  //

  def processWs(f: Pipe[F, WebSocketFrame, WebSocketFrame]): F[Response[F]] = {
    Queue.unbounded[F, WebSocketFrame].flatMap { q =>
      WebSocketBuilder[F].build(q.dequeue.through(f), q.enqueue)
    }
  }

  def processMsg(f: Pipe[F, MsgInputs, Msg]): F[Response[F]] = {
    val wrapFrame: Pipe[F, Msg, WebSocketFrame] = _.collect { case m => Text(m.asJson.noSpaces) }
    processWs(wrapFrame compose f compose pipeWsMsg[MsgInputs])
  }

  //


  def routes(state: State[F]): HttpRoutes[F] = HttpRoutes.of[F] {
    case request @ GET -> Root / "ok" => Ok("OK")

    case GET -> Root / "debug" / "echo" => processWs(pipeWsStr compose pipeWsStrings)
    case GET -> Root / "debug" / "json" => processWs(pipeWsStr compose pipeWsJson)

    case GET -> Root / "lobby_api" =>
      LobbyApi[F](state.updates).flatMap(l =>
        processMsg(l.process))
  }

  def buildStream: F[Stream[F, ExitCode]] =
    for (state <- State())
      yield BlazeServerBuilder[F]
        .bindHttp(9000)
        .withHttpApp(routes(state).orNotFound)
        .serve
}

object WSApp {
  def apply[F[+_]: ConcurrentEffect: Timer]: WSApp[F] = new WSApp[F]
}
