package example.session

import cats._
import cats.implicits._
import cats.effect._
import fs2.concurrent.Topic
import scala.concurrent.ExecutionContext

import example.model._
import example.state.DbModel._

object Types {
  type Updates[F[_]] = Topic[F, Option[Msg]]
}

import Types._

// Global
case class State[F[_]](
  updates: Updates[F]
)

object State {
  implicit val contextShift: ContextShift[IO] = IO.contextShift(ExecutionContext.global)

  def apply[F[_]]()(implicit F: ConcurrentEffect[F]): F[State[F]] =
    for (updates <- Topic[F, Option[Msg]](None))
      yield new State[F](updates)
}

// Lasts for a client's websocket session
case class Session[F[_]](
  authorized: Boolean,
  subscribed: Boolean,
  user: Option[User]
)
