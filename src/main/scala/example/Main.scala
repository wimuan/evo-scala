package example

import cats.effect._
import cats.implicits._
import scala.concurrent.Await
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.sys.process.Process

import example.wsapp.WSApp
import example.state._

object Main extends IOApp {
  def run(args: List[String]): IO[ExitCode] = {
    val alert = Process(List("sh", "-c", "command -v alert > /dev/null && alert # server started"))

    for {
      _ <- IO(alert!)
      _ <- IO(Await.result(Db.setup(), 5 second)) // may fail. if so, tough luck
      _ <- WSApp[IO].buildStream.flatMap(_.compile.drain.as(ExitCode.Success))
    } yield ExitCode.Success
  }
}
