# candidate test assignment

Time left: 2 hours 59 minutes

You have been assigned the task to implement a server for a JSON-based Lobby API protocol served over REST or WebSocket.
Choose WebSocket for extra points, but going with REST is fine as well (if REST is chosen, WebSocket sections below can be ignored and vice versa).

Please clearly document how to launch the server and follow software development best practices. Good luck!

# Solution description

## Lobby API
### Overview
REST
<hr>
The server should be hosted on port 9000. URLs for different operations should be defined under the base URL /lobby\_api following the best practices of REST API design.
The request and response models, described below, are more suitable for WebSocket than for REST, so they can be adjusted as needed.

WebSocket
<hr>
The server should be hosted on port 9000. All operations should flow through the same base URL /lobby\_api.

### Authenticating
The server should support two user types: "admin" and "user".

REST
<hr>
HTTP basic authentication should be used for every request.

WebSocket
<hr>
The following authentication flow should be implemented. The client sends:

    {
        "$type": "login",
        "username": "user1234",
        "password": "password1234"
    }

The server responds:

    {
        "$type": "login_failed"
    }

Or:

    {
        "$type": "login_successful",
        "user_type": "admin"
    }

### Pinging the server

The client can ping the server to check the connectivity. The ping request includes the sequence number, which allows to trace the exact ping duration:

    {
        "$type": "ping",
        "seq": 1
    }

The server responds:

    {
        "$type": "pong",
        "seq": 1
    }

### Getting or subscribing to the list of tables

REST
<hr>
The client executes an appropriate HTTP request and the server responds with the list of tables:

    {
      "$type": "table_list",
      "tables": [
        {
          "id": 1,
          "name": "table - James Bond",
          "participants": 7
        },
        {
          "id": 2,
          "name": "table - Mission Impossible",
          "participants": 4
        }
      ]
    }

The client sends:

    {
      "$type": "subscribe_tables"
    }

The server immediately responds with the list of tables:

    {
      "$type": "table_list",
      "tables": [
        {
          "id": 1,
          "name": "table - James Bond",
          "participants": 7
        },
        {
          "id": 2,
          "name": "table - Mission Impossible",
          "participants": 4
        }
      ]
    }

The server then updates the client with `table_added`, `table_removed` and `table_updated` messages whenever there are changes to the list of tables.

### When a table is added:

    {
      "$type": "table_added",
      "after_id": 1,
      "table": {
        "id": 3,
        "name": "table - Foo Fighters",
        "participants": 9
      }
    }

### When a table is updated:

    {
      "$type": "table_updated",
      "table": {
        "id": 3,
        "name": "table - Foo Fighters",
        "participants": 9
      }
    }

### When a table is removed:

    {
      "$type": "table_removed",
      "id": 1
    }

### Finally, the client can unsubscribe from the list of tables at any time:

    {
      "$type": "unsubscribe_tables"
    }

No response from the server is expected upon unsubcription request. Moreover, once it is processed, the server must stop sending table_added, table_removed and table_updated messages to the client.

## Privileged commands

Only the admin is allowed to use these commands, otherwise the server responds:

    {
      "$type": "not_authorized"
    }

### Adding a table
The client sends:

    {
      "$type": "add_table",
      "after_id": 1,
      "table": {
        "id": 3,
        "name": "table - Foo Fighters",
        "participants": 4
      }
    }

If after_id is -1, the table must be added at the beginning of the list of tables.

### Updating a table
The client sends:

    {
      "$type": "update_table",
      "table": {
        "id": 3,
        "name": "table - Foo Fighters",
        "participants": 5
      }
    }

### Removing a table
The client sends:

    {
      "$type": "remove_table",
      "id": 3
    }

### UI updates
When the admin makes changes to a table, the client must do an optimistic UI update. However, in case the server responds with a failure, the optimistic UI update must be reverted.

Possible failures:

    {
      "$type": "removal_failed",
      "id": 3
    }

    {
      "$type": "update_failed",
      "id": 3
    }
