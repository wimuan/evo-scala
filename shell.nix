with (import <nixpkgs> {});

# built on nixos-unstable at 2020-03-27
# let current = builtins.readFile <nixpkgs/.version>;
# in assert (builtins.compareVersions "20.09" current) != 1;

mkShell {
  buildInputs = [ sbt ws sqlite ];
}
