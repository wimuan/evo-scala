sealed trait X

sealed trait XX extends X
sealed trait XY extends X

final case class XXX() extends XX
final case class XXY() extends XX

final case class XYX() extends XY
final case class XYY() extends XY

val f : X => Int = {
  case a: XX => 1
  case a: XY => 2
}

val g : X => Int = {
  case a: XX => 1
  case a: XY => 2
}
