case class Codec[A](a: A, type_ : String) {
  def render: String = s"I have this ${type_}: ${a.toString}"
}

def use[A](a: A)(implicit c: Codec[A]) = c.render

implicit val codecInt: Codec[Int] = Codec(1, "number")
implicit val codecString: Codec[String] = Codec("rabbit", "string")

use(1)
use("string")
